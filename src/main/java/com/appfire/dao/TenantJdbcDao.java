/*
 * Copyright (c) 2016 Appfire Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.appfire.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.appfire.model.Tenant;
import com.appfire.service.TenantService;
import com.atlassian.jwt.exception.JwtIssuerLacksSharedSecretException;
import com.atlassian.jwt.exception.JwtUnknownIssuerException;

@Repository
public class TenantJdbcDao implements TenantService {

	private JdbcTemplate template;

	private SimpleJdbcInsert insert;

	private RowMapper<Tenant> mapper = BeanPropertyRowMapper.newInstance(Tenant.class);

	@Autowired
	public TenantJdbcDao(JdbcTemplate template) {
		this.template = template;
		insert = new SimpleJdbcInsert(template).withTableName("TenantInformation").usingGeneratedKeyColumns("id");
	}

	@Override
	public Tenant byClientKey(String clientKey) {
		return template.queryForObject("SELECT * FROM TenantInformation WHERE client_key = ?", mapper, clientKey);
	}

	@Override
	public void update(Tenant info) {
		info.setId(insert.executeAndReturnKey(new BeanPropertySqlParameterSource(info)).toString());
	}

	@Override
	public String getSharedSecret(String issuer) throws JwtIssuerLacksSharedSecretException, JwtUnknownIssuerException {

		String sql = "SELECT shared_secret FROM TenantInformation WHERE client_key = ?";
		return template.queryForObject(sql, String.class, issuer);
	}

	@Override
	public boolean isValid(String issuer) {
		String sql = "SELECT COUNT(client_key) FROM TenantInformation WHERE client_key = ?";
		return template.queryForObject(sql, Integer.class, issuer) == 1;
	}
}
