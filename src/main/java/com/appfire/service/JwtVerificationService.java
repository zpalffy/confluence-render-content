/*
 * Copyright (c) 2016 Appfire Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.appfire.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appfire.model.Tenant;
import com.atlassian.jwt.CanonicalHttpRequest;
import com.atlassian.jwt.Jwt;
import com.atlassian.jwt.core.http.JavaxJwtRequestExtractor;
import com.atlassian.jwt.core.reader.JwtClaimVerifiersBuilder;
import com.atlassian.jwt.core.reader.NimbusJwtReaderFactory;
import com.atlassian.jwt.exception.JwtIssuerLacksSharedSecretException;
import com.atlassian.jwt.exception.JwtParseException;
import com.atlassian.jwt.exception.JwtUnknownIssuerException;
import com.atlassian.jwt.exception.JwtVerificationException;
import com.atlassian.jwt.reader.JwtClaimVerifier;
import com.atlassian.jwt.reader.JwtReaderFactory;

/**
 * A service used to verify incoming JWT tokens. Generating outbound JWT tokens
 * should be done with the Tenant object.
 */
@Service
public class JwtVerificationService {

	private TenantService tenantSvc;

	@Autowired
	public JwtVerificationService(TenantService tenantSvc) {
		this.tenantSvc = tenantSvc;
	}

	public Tenant tenant(Jwt jwt) {
		return tenantSvc.byClientKey(jwt.getIssuer());
	}

	public Jwt verifyRequest(HttpServletRequest request) throws UnsupportedEncodingException,
			NoSuchAlgorithmException, JwtVerificationException,
			JwtIssuerLacksSharedSecretException, JwtUnknownIssuerException, JwtParseException,
			JwtNoTokenException {

		JwtReaderFactory jwtReaderFactory = new NimbusJwtReaderFactory(tenantSvc, tenantSvc);
		JavaxJwtRequestExtractor jwtRequestExtractor = new JavaxJwtRequestExtractor();
		CanonicalHttpRequest canonicalHttpRequest = jwtRequestExtractor
				.getCanonicalHttpRequest(request);
		Map<String, ? extends JwtClaimVerifier> requiredClaims = JwtClaimVerifiersBuilder
				.build(canonicalHttpRequest);

		String jwt = jwtRequestExtractor.extractJwt(request);
		if (jwt == null) {
			throw new JwtNoTokenException();
		}

		return jwtReaderFactory.getReader(jwt).readAndVerify(jwt, requiredClaims);
	}

	public static class JwtNoTokenException extends JwtParseException {

		private static final long serialVersionUID = 1L;

		public JwtNoTokenException() {
			super("Cannot authenticate a request without a JWT token");
		}
	}
}
