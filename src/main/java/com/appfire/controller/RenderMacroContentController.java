/*
 * Copyright (c) 2016 Appfire Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.appfire.controller;

import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.appfire.model.Tenant;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

@Controller
public class RenderMacroContentController extends AbstractController {

	private static final Logger LOG = LoggerFactory.getLogger(RenderMacroContentController.class);

	private int requestTimeout = 10000;

	private Gson gson = new GsonBuilder().setPrettyPrinting().create();

	// extremely simple GET request with jwt:
	private String restGet(Tenant tenant, HttpServletRequest req, String endpoint) throws Exception {
		URI uri = new URIBuilder(getAtlassianUrl(req) + endpoint).addParameter("jwt",
				tenant.createJwtToken(HttpMethod.GET, req.getParameter("cp"), endpoint, null)).build();

		LOG.info("Requesting uri {}", uri);
		HttpResponse response = Request.Get(uri).connectTimeout(requestTimeout).execute().returnResponse();

		return EntityUtils.toString(response.getEntity());
	}

	private String restPost(Tenant tenant, HttpServletRequest req, String endpoint, List<Pair<String, String>> parms,
			String body) throws Exception {

		LOG.debug("POST action, body: {}" + body);
		URIBuilder builder = new URIBuilder(getAtlassianUrl(req) + endpoint);

		Map<String, String[]> parmMap = Maps.newHashMap();
		for (Pair<String, String> pair : parms) {
			builder.addParameter(pair.getKey(), pair.getValue());
			String[] vals = parmMap.get(pair.getKey());

			parmMap.put(pair.getKey(),
					vals == null ? new String[] { pair.getValue() } : ArrayUtils.add(vals, pair.getValue()));
		}

		HttpResponse response = Request
				.Post(builder.build())
				.bodyString(body, ContentType.APPLICATION_JSON)
				.addHeader("Authorization",
						"JWT " + tenant.createJwtToken(HttpMethod.POST, req.getParameter("cp"), endpoint, parmMap))
				.execute().returnResponse();
		return EntityUtils.toString(response.getEntity());
	}

	@RequestMapping(path = "/render-server", method = RequestMethod.GET)
	public ModelAndView showCommandPage(Map<String, Object> model, HttpServletRequest req, @RequestParam String pageId,
			@RequestParam String pageVersion, @RequestParam String macroId) throws Exception {

		// validate and obtain tenant info:
		Tenant tenant = tenant(validate(req));

		// request the macro contents:
		// /confluence/rest/api/content/{id}/history/{version}/macro/id/{macroId}
		String raw = restGet(tenant, req, "/rest/api/content/" + pageId + "/history/" + pageVersion + "/macro/id/"
				+ macroId);

		LOG.debug("Raw json return value: {}", raw);

		JsonObject jobj = gson.fromJson(raw, JsonObject.class);

		JsonObject conversion = new JsonObject();
		conversion.addProperty("value", gson.fromJson(jobj.get("body"), String.class));
		conversion.addProperty("representation", "storage");

		JsonObject content = new JsonObject();
		content.addProperty("id", pageId);
		conversion.add("content", content);

		List<Pair<String, String>> parms = ImmutableList.of(Pair.of("expand",
				"webresource.superbatch.tags.all,webresource.tags.all"));
		JsonObject result = gson.fromJson(
				restPost(tenant, req, "/rest/api/contentbody/convert/view", parms, conversion.toString()),
				JsonObject.class);
		LOG.debug("Render request JSON: {}", gson.toJson(result));

		return new ModelAndView("render-server").addObject("result", result);
	}
}
