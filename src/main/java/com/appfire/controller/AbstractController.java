/*
 * Copyright (c) 2016 Appfire Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.appfire.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.appfire.model.Tenant;
import com.appfire.service.JwtVerificationService;
import com.atlassian.jwt.Jwt;
import com.atlassian.jwt.exception.JwtIssuerLacksSharedSecretException;
import com.atlassian.jwt.exception.JwtParseException;
import com.atlassian.jwt.exception.JwtUnknownIssuerException;
import com.atlassian.jwt.exception.JwtVerificationException;

public abstract class AbstractController {

	private JwtVerificationService svc;

	@Autowired
	public void setJwtService(JwtVerificationService svc) {
		this.svc = svc;
	}

	@ModelAttribute("atlassianUrl")
	public String getAtlassianUrl(HttpServletRequest req) {
		String str = req.getParameter("xdm_e");

		if (str != null) {
			StringBuilder sb = new StringBuilder(str);

			str = req.getParameter("cp");
			if (str != null) {
				sb.append(str);
			}

			return sb.toString();
		}

		return null;
	}

	protected Tenant tenant(Jwt jwt) {
		return svc.tenant(jwt);
	}

	protected Jwt validate(HttpServletRequest req) throws UnsupportedEncodingException,
			NoSuchAlgorithmException, JwtVerificationException,
			JwtIssuerLacksSharedSecretException, JwtUnknownIssuerException, JwtParseException {

		return svc.verifyRequest(req);
	}
}
