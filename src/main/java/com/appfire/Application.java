/*
 * Copyright (c) 2016 Appfire Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.appfire;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.ImmutableSet;

@Controller
@SpringBootApplication
public class Application {

	private static final Set<Integer> KNOWN_PORTS = ImmutableSet.of(80, 443);

	@Value("${info.app.key}")
	private String appKey;

	@Value("${info.app.name}")
	private String appName;

	@Value("${info.app.baseUrl}")
	private String baseUrl;

	@RequestMapping(path = "/atlassian-connect", method = RequestMethod.GET, produces = "application/json")
	public ModelAndView atlassianConnect(HttpServletRequest req, HttpServletResponse resp) {

		resp.setContentType("application/json");
		String base = baseUrl;

		if (StringUtils.isEmpty(base)) {
			String scheme = StringUtils.defaultString(req.getHeader("X-Forwarded-Proto"), req.getScheme());
			StringBuilder sb = new StringBuilder(scheme).append("://").append(req.getServerName());

			if (!KNOWN_PORTS.contains(req.getServerPort())) {
				sb.append(':').append(req.getServerPort());
			}

			base = sb.toString();
		}

		return new ModelAndView("atlassian-connect").addObject("appKey", appKey).addObject("appName", appName)
				.addObject("baseUrl", base);
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
