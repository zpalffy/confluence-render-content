/*
 * Copyright (c) 2016 Appfire Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 CREATE TABLE TenantInformation (
	key varchar(255) not null,
	client_key varchar(255) not null,
	public_key varchar(255),
	shared_secret varchar(255) not null,
	server_version varchar(10),
	plugins_version varchar(10),
	base_url varchar(255),
	product_type varchar(255),
	description varchar(255),
	service_entitlement_number varchar(255),
	event_type varchar(20)
);