confluence-render-content
=========================

This is a [Confluence](https://www.atlassian.com/software/confluence/) macro that uses the [Atlassian Connect framework](https://developer.atlassian.com/static/connect/docs/latest/guides/introduction.html) to render its macro body.  It is a simple test case to explore the AC framework and its interactions with the [Confluence REST API's](https://docs.atlassian.com/atlassian-confluence/REST/latest/).  The service itself is written in [Java](http://www.oracle.com/technetwork/es/java/javase/overview/index.html) and uses the [Spring Boot framework](http://projects.spring.io/spring-boot/).  All REST calls are made on the server on behalf of the add-on user.  For this reason the add-on maintains tenant data for each Confluence instance it is installed on.

Running the Add-on
--------

1. clone the repository and `cd` into the repo directory.
2. `./gradlew bootrun`

The add-on should now be running on port 8080.  You can verify the service is up and running by visiting http://localhost:8080/atlassian-connect.json and making sure you see valid JSON content.  If you need to specify an alternate port, see http://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/#howto-change-the-http-port.

By default, the add-on uses an in-memory database to store its tenant data.  This means that if the add-on is restarted for any reason (e.g. a restart or a hot reload) all db data is lost and therefore the add-on will no longer be able to make authenticated REST calls back to the Confluence instance.  To work around this, you can use the file-based version of the database by setting an environment variable (feel free to change the absolute path to something else.)

    export SPRING_DATASOURCE_URL=jdbc:hsqldb:file:/tmp/hsql/render-contents
    
All standard Spring Boot actions and tasks are available as well.  For example, you can invoke `./gradlew clean build` and then run the add-on from the resulting jar if desired.

Installing the Add-on
------------

1. Install and start [ngrok](https://ngrok.com/), e.g. `ngrok http 8080`
2. Verify you can access the metadata endpoint via the https tunnel: `https://<YOUR_NGROK_ADDRESS>/atlassian-connect.json`
3. Verify the `baseUrl` property in the JSON from the previous step is the same as the ngrok address you were assigned: `https://<YOUR_NGROK_ADDRESS>`
4. Install the add-on in a running Confluence instance using the json file url from step 2.  See https://developer.atlassian.com/static/connect/docs/latest/guides/development-setup.html

Keep in mind that any time ngrok is restarted, the url assigned by the ngrok service has likely changed.  In that event, you will need to uninstall and reinstall the add-on via UPM.

Using the Macro
-------

You should now be able to create a macro instance with some content.  To do this, create a new page or edit an existing one.  Then through the macro browser select the "Render Macro Content" macro and add some content.  When the content is rendered, it will go through the add-on and attempt to use the REST API's to pull in the HTML content as well as the associated webresources.

Debugging
---------

The REST calls and processing logic are all in the `com.appfire.controller.RenderMacroContentController` class.  The rest of the code is either for Spring Boot or Atlassian Connect support.  The class performs the following:

1. Retrieves the macro content
2. Takes the macro content in storage format and retrieves the view as well as corresponding web resources.
3. Renders the page with the given HTML and web resources. 